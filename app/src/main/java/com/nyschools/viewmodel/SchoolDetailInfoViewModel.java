package com.nyschools.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyschools.BR;
import com.nyschools.listeners.NYCServiceListener;
import com.nyschools.model.ErrorObject;
import com.nyschools.model.NYCSchoolDetailInfo;
import com.nyschools.model.NYCSchoolMoreDetail;
import com.nyschools.services.NYCServiceProvider;
import com.nyschools.utils.Constants;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by vino on 18-Apr-19.
 */

public class SchoolDetailInfoViewModel extends BaseObservable implements NYCServiceListener{

    private Activity activity;
    private NYCSchoolMoreDetail nycSchoolMoreDetail;

    public SchoolDetailInfoViewModel(Activity activity){
        this.activity = activity;
    }

    public void fetchSchoolDetail(String dbn) {
        NYCServiceProvider apiServiceProvider = NYCServiceProvider.getInstance(activity);
        apiServiceProvider.getSchoolDetails(this,dbn);
    }

    @Bindable
    public NYCSchoolMoreDetail getMoreDetail() {
        return nycSchoolMoreDetail;
    }


    public void setMoreDetail(NYCSchoolMoreDetail moreDetail) {
        this.nycSchoolMoreDetail = moreDetail;
        fetchSchoolDetail(moreDetail.getAdditionalInfo().getDbn());
    }


    @Override
    public void onResponseSuccess(Response response, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.NYC_SCHOOL_DETAILS:
                ArrayList<NYCSchoolDetailInfo> detail = (ArrayList<NYCSchoolDetailInfo>)response.body();
                if(!detail.isEmpty()) {
                    nycSchoolMoreDetail.setDetailInfo(detail.get(0));
                    notifyPropertyChanged(BR.moreDetail);
                }
                break;
        }
    }

    @Override
    public void onResponseError(ErrorObject errorObject, Throwable throwable, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.NYC_SCHOOL_LIST:
                Toast.makeText(activity,"SchoolList: error response: " + new Gson().toJson(errorObject),Toast.LENGTH_SHORT).show();
                break;
            case Constants.ApiFlags.NYC_SCHOOL_DETAILS:
                Toast.makeText(activity,"SchoolDetail: error response: " + new Gson().toJson(errorObject),Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
