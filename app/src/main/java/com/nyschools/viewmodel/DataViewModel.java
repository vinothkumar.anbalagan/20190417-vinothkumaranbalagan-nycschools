package com.nyschools.viewmodel;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.nyschools.BR;
import com.nyschools.adapter.NYCSchoolAdapter;
import com.nyschools.listeners.NYCServiceListener;
import com.nyschools.model.ErrorObject;
import com.nyschools.model.NYCSchoolDetailInfo;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.services.NYCServiceProvider;
import com.nyschools.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Response;

public class DataViewModel extends BaseObservable implements NYCServiceListener {
    private static final String TAG = "DataViewModel";
    private NYCSchoolAdapter adapter;
    private List<NYCSchoolListInfo> data;
    private Activity activity;

    public DataViewModel(Activity activity) {
        this.activity = activity;
        data = new ArrayList<>();
        adapter = new NYCSchoolAdapter();
        setUp();
    }

    public void setUp() {
        // perform set up tasks, such as adding listeners, data population, etc.
        NYCServiceProvider apiServiceProvider = NYCServiceProvider.getInstance(activity);
        //EventBusUtil.checkAndRegister(this);
        apiServiceProvider.getSchoolList(this);
    }

    public void tearDown() {
        // perform tear down tasks, such as removing listeners
    }

    @Bindable
    public List<NYCSchoolListInfo> getData() {
        return this.data;
    }

    @Bindable
    public NYCSchoolAdapter getAdapter() {
        return this.adapter;
    }

    @Override
    public void onResponseSuccess(Response response, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.NYC_SCHOOL_LIST:
                ArrayList<NYCSchoolListInfo> listLiveData = (ArrayList<NYCSchoolListInfo>)response.body();
                data.addAll(listLiveData);
                notifyPropertyChanged(BR.data);
                break;
            case Constants.ApiFlags.NYC_SCHOOL_DETAILS:
               List<NYCSchoolDetailInfo> detail = (List<NYCSchoolDetailInfo>)response.body();

                break;
        }
    }

    @Override
    public void onResponseError(ErrorObject errorObject, Throwable throwable, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.NYC_SCHOOL_LIST:
                //Toast.makeText(this,"SchoolList: error response: " + new Gson().toJson(errorObject),Toast.LENGTH_SHORT).show();
                break;
            case Constants.ApiFlags.NYC_SCHOOL_DETAILS:
                //Toast.makeText(this,"SchoolDetail: error response: " + new Gson().toJson(errorObject),Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
