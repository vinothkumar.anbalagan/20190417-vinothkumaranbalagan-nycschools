package com.nyschools.viewmodel;

import android.app.Activity;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.services.NYCServiceProvider;

public class DataItemViewModel extends BaseObservable{
    private NYCSchoolListInfo dataModel;

    public DataItemViewModel(NYCSchoolListInfo dataModel) {
        this.dataModel = dataModel;
    }


    @Bindable
    public String getTitle() {
        return !TextUtils.isEmpty(dataModel.getSchoolName()) ? dataModel.getSchoolName() : "";
    }
}
