package com.nyschools.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.nyschools.R;
import com.nyschools.databinding.ItemDataBinding;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.viewmodel.DataItemViewModel;

import java.util.ArrayList;
import java.util.List;

public class NYCSchoolAdapter extends RecyclerView.Adapter<NYCSchoolAdapter.DataViewHolder> {
    private static final String TAG = "NYCSchoolAdapter";
    private ArrayList<NYCSchoolListInfo> data;

    public NYCSchoolAdapter() {
        this.data = new ArrayList<>();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data,
                new FrameLayout(parent.getContext()), false);
        return new DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        final NYCSchoolListInfo dataModel = data.get(position);
        holder.setViewModel(new DataItemViewModel(dataModel));
        holder.itemView.findViewById(R.id.school_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Item " + dataModel.getSchoolName(),Toast.LENGTH_LONG).show();
               // ((SchoolInfoListActivity)v.getContext()).showSchoolDetail(dataModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    @Override
    public void onViewAttachedToWindow(DataViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(DataViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<NYCSchoolListInfo> data) {
        if (data == null || data.isEmpty()) {
            this.data.clear();
        } else {
            this.data.addAll(data);
        }
        notifyDataSetChanged();
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {
        ItemDataBinding binding;

        DataViewHolder(View itemView) {
            super(itemView);
            bind();
        }

         void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

        void setViewModel(DataItemViewModel viewModel) {
            if (binding != null) {
                binding.setViewModel(viewModel);
            }
        }
    }
}
