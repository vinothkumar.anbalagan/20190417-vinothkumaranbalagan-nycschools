package com.nyschools.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyschools.R;
import com.nyschools.SchoolInfoDetailActivity;
import com.nyschools.SchoolInfoDetailFragment;
import com.nyschools.SchoolInfoListActivity;
import com.nyschools.dummy.DummyContent;

import java.util.List;

/**
 * Created by vino on 18-Apr-19.
 */

public class SimpleItemRecyclerViewAdapter
        extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

    private final SchoolInfoListActivity mParentActivity;
    private final List<DummyContent.DummyItem> mValues;
    private final boolean mTwoPane;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DummyContent.DummyItem item = (DummyContent.DummyItem) view.getTag();
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(SchoolInfoDetailFragment.ARG_ITEM_ID, item.id);
                SchoolInfoDetailFragment fragment = new SchoolInfoDetailFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.schoolinfo_detail_container, fragment)
                        .commit();
            } else {
                Context context = view.getContext();
                Intent intent = new Intent(context, SchoolInfoDetailActivity.class);
                intent.putExtra(SchoolInfoDetailFragment.ARG_ITEM_ID, item.id);
                context.startActivity(intent);
            }
        }
    };

    SimpleItemRecyclerViewAdapter(SchoolInfoListActivity parent,
                                  List<DummyContent.DummyItem> items,
                                  boolean twoPane) {
        mValues = items;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schoolinfo_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

      /*  holder.s.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);*/
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;


        ViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.school_name);

        }
    }
}
