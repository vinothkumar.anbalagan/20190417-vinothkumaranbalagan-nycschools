package com.nyschools.listeners;


import com.nyschools.model.ErrorObject;
import retrofit2.Response;

public interface NYCServiceListener {

    void onResponseSuccess(Response response, int apiFlag);

    void onResponseError(ErrorObject errorObject, Throwable throwable, int apiFlag);

}
