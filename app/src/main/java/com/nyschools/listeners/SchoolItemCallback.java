package com.nyschools.listeners;


import com.nyschools.model.NYCSchoolListInfo;

public interface SchoolItemCallback {
    void onClick(NYCSchoolListInfo schoolInfo);
}
