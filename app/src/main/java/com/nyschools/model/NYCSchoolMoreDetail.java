package com.nyschools.model;

/**
 * Created by vino on 18-Apr-19.
 */

public class NYCSchoolMoreDetail {

    private NYCSchoolListInfo additionalInfo;
    private NYCSchoolDetailInfo detailInfo;

    public NYCSchoolListInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(NYCSchoolListInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public NYCSchoolDetailInfo getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(NYCSchoolDetailInfo detailInfo) {
        this.detailInfo = detailInfo;
    }
}
