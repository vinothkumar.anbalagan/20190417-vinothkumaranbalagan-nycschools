package com.nyschools.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class NYCSchoolListInfo implements Serializable{

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("website")
    private String website;

    @SerializedName("school_email")
    private String email;


    @SerializedName("primary_address_line_1")
    private String address;

    @SerializedName("city")
    private String city;

    @SerializedName("zip")
    private String zip;

    public String getDbn() {
        return dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getWebsite() {
        return website;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }
}
