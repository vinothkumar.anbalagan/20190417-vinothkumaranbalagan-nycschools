package com.nyschools.model;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NYCSchoolDetailInfo implements Serializable{

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("sat_critical_reading_avg_score")
    private String readingAvgScore;

    @SerializedName("sat_math_avg_score")
    private String mathAvgScore;

    @SerializedName("sat_writing_avg_score")
    private String mathWritingAvgScore;

    @SerializedName("num_of_sat_test_takers")
    private String testTaker;

    public String getDbn() {
        return dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getReadingAvgScore() {
        return readingAvgScore;
    }

    public String getMathAvgScore() {
        return mathAvgScore;
    }

    public String getMathWritingAvgScore() {
        return mathWritingAvgScore;
    }

    public String getTestTaker() {
        return testTaker;
    }
}
