package com.nyschools;

import android.app.Application;
import android.databinding.DataBindingUtil;

import com.nyschools.databinding.AppDataBindingComponent;


public class NYCSchoolApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DataBindingUtil.setDefaultComponent(new AppDataBindingComponent());
    }
}
