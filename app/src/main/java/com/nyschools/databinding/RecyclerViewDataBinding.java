package com.nyschools.databinding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import com.nyschools.adapter.NYCSchoolAdapter;
import com.nyschools.model.NYCSchoolListInfo;
import java.util.List;

/**
 * Created by vino on 17-Apr-19.
 */

public class RecyclerViewDataBinding {

    @BindingAdapter({"app:adapter", "app:data"})
    public void bind(RecyclerView recyclerView, NYCSchoolAdapter adapter, List<NYCSchoolListInfo> data) {
        recyclerView.setAdapter(adapter);
        adapter.updateData(data);
    }
}
