package com.nyschools.databinding;

/**
 * Created by vino on 17-Apr-19.
 */
public class AppDataBindingComponent implements android.databinding.DataBindingComponent {
    @Override
    public RecyclerViewDataBinding getRecyclerViewDataBinding() {
        return new RecyclerViewDataBinding();
    }
}
