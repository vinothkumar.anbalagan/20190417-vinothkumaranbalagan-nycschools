package com.nyschools.services;

import com.nyschools.model.NYCSchoolDetailInfo;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.utils.Constants;

import java.util.List;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET(Constants.UrlPath.SCHOOL_LIST_SERVICES)
    Call<List<NYCSchoolListInfo>> getNYCSchoolList();

    @GET(Constants.UrlPath.SCHOOL_DETAIL_SERVICES)
    Call<List<NYCSchoolDetailInfo>> getNYCSchoolDetail(@Query("dbn") String dbn);

}
