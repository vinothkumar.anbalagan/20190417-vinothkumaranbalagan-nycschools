package com.nyschools.services;

import android.content.Context;

import com.nyschools.listeners.NYCServiceListener;
import com.nyschools.model.NYCSchoolDetailInfo;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.utils.Constants;
import com.nyschools.utils.HttpUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NYCServiceProvider  extends BaseApiServices {

    private static NYCServiceProvider apiServiceProvider;
    private ApiServices apiServices;

    private NYCServiceProvider(Context context) {
        super(context, true);
        apiServices = retrofit.create(ApiServices.class);
    }

    public static NYCServiceProvider getInstance(Context context) {
        if (apiServiceProvider == null) {
            apiServiceProvider = new NYCServiceProvider(context);
        }
        return apiServiceProvider;
    }


    public void getSchoolList(final NYCServiceListener retrofitListener) {
        Call<List<NYCSchoolListInfo>> call = apiServices.getNYCSchoolList();
        call.enqueue(new Callback<List<NYCSchoolListInfo>>() {
            @Override
            public void onResponse(Call<List<NYCSchoolListInfo>> call, Response<List<NYCSchoolListInfo>> response) {
                validateResponse(response, retrofitListener, Constants.ApiFlags.NYC_SCHOOL_LIST);
            }

            @Override
            public void onFailure(Call<List<NYCSchoolListInfo>> call, Throwable t) {
                retrofitListener.onResponseError(HttpUtil.getServerErrorPojo(context), t, Constants.ApiFlags.NYC_SCHOOL_LIST);
            }
        });
    }

    public void getSchoolDetails(final NYCServiceListener retrofitListener,String dbn) {
        Call<List<NYCSchoolDetailInfo>> call = apiServices.getNYCSchoolDetail(dbn);
        call.enqueue(new Callback<List<NYCSchoolDetailInfo>>() {
            @Override
            public void onResponse(Call<List<NYCSchoolDetailInfo>> call, Response<List<NYCSchoolDetailInfo>> response) {
                validateResponse(response, retrofitListener, Constants.ApiFlags.NYC_SCHOOL_DETAILS);
            }

            @Override
            public void onFailure(Call<List<NYCSchoolDetailInfo>> call, Throwable t) {
                //pass correct flag to differentiate between multiple api calls in same activity/frag
                retrofitListener.onResponseError(HttpUtil.getServerErrorPojo(context), t, Constants.ApiFlags.NYC_SCHOOL_DETAILS);
            }
        });
    }
}
