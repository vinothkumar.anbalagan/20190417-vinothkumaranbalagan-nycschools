package com.nyschools;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nyschools.databinding.SchoolinfoDetailBinding;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.model.NYCSchoolMoreDetail;
import com.nyschools.viewmodel.SchoolDetailInfoViewModel;

/**
 * Created by vino on 18-Apr-19.
 */
public class SchoolInfoDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    private NYCSchoolListInfo mItem;

    public SchoolInfoDetailFragment() {
    }

    private NYCSchoolMoreDetail  moreDetail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = (NYCSchoolListInfo)getArguments().getSerializable(ARG_ITEM_ID);
            moreDetail = new NYCSchoolMoreDetail();
            if(mItem != null) {
                moreDetail.setAdditionalInfo(mItem);
            }
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle("NYCSchool Detail Info");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SchoolinfoDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.schoolinfo_detail, container, false);
        SchoolDetailInfoViewModel viewModel = new SchoolDetailInfoViewModel(getActivity());
        binding.setDetailviewModel(viewModel);
        viewModel.setMoreDetail(moreDetail);
        return binding.getRoot();
    }
}
