package com.nyschools.utils;
public class Constants {
    public static final String SELECTED_SCHOOL_INFO = "selectedSchoolInfo";

    public interface TimeOut {
        int IMAGE_UPLOAD_CONNECTION_TIMEOUT = 120;
        int IMAGE_UPLOAD_SOCKET_TIMEOUT = 120;
        int SOCKET_TIME_OUT = 60;
        int CONNECTION_TIME_OUT = 60;
    }

    public interface UrlPath {
        String SCHOOL_LIST_SERVICES = "s3k6-pzi2.json";
        String SCHOOL_DETAIL_SERVICES = "f9bf-2cp4.json";
    }

    //Need unique flags for all apis in case if hitting multiple apis in same activity/fragment
    public interface ApiFlags {
        int NYC_SCHOOL_LIST = 0;
        int NYC_SCHOOL_DETAILS = 1;
    }

    public interface ErrorClass {
        String CODE = "code";
        String STATUS = "status";
        String MESSAGE = "message";
        String DEVELOPER_MESSAGE = "developerMessage";
    }
}
