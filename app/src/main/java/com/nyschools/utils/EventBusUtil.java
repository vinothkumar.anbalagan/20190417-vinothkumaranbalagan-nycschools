package com.nyschools.utils;

import de.greenrobot.event.EventBus;

/**
 * Utility class to handle EventBus
 */
public final class EventBusUtil {

    private EventBusUtil(){
        //Private constructor
    }

    /**
     * Method to check whether Event Bus is registered for this class object or not.
     * If not it registers the new one.
     *
     * @param subscriber - object needs to registered
     */
    public static void checkAndRegister(Object subscriber) {
        if (!EventBus.getDefault().isRegistered(subscriber)) {
            EventBus.getDefault().register(subscriber);
        }
    }

    /**
     * Method to unregister Event Bus if it is registered for this class object or not.
     *
     * @param subscriber - object needs to unregistered
     */
    public static void checkAndUnRegister(Object subscriber) {
        if (EventBus.getDefault().isRegistered(subscriber)) {
            EventBus.getDefault().unregister(subscriber);
        }
    }
}
