package com.nyschools.utils;

public class ConfigUtils {
    public static final String SERVER_URL = "https://data.cityofnewyork.us/";
    public static final int SERVER_PORT = 8080;
    public static final String APPLICATION_BASE_URL = "resource/";
}
