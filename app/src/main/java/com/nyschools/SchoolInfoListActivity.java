package com.nyschools;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.nyschools.databinding.ActivitySchoolinfoListBinding;
import com.nyschools.dummy.DummyContent;
import com.nyschools.listeners.NYCServiceListener;
import com.nyschools.model.ErrorObject;
import com.nyschools.model.NYCSchoolDetailInfo;
import com.nyschools.model.NYCSchoolListInfo;
import com.nyschools.services.NYCServiceProvider;
import com.nyschools.utils.Constants;
import com.nyschools.viewmodel.DataViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

public class SchoolInfoListActivity extends AppCompatActivity implements NYCServiceListener {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private RecyclerView recyclerView;
    private static ArrayList<NYCSchoolListInfo> mValues;
    private SimpleItemRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_schoolinfo_list);
        //ActivitySchoolinfoListBinding binding =DataBindingUtil.setContentView(this, R.layout.activity_schoolinfo_list);
        DataViewModel dataViewModel = new DataViewModel(this);
        //binding.setViewModel(dataViewModel);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());


        if (findViewById(R.id.schoolinfo_detail_container) != null) {
            mTwoPane = true;
        }

        recyclerView = findViewById(R.id.schoolinfo_list);
        assert recyclerView != null;
        if(mValues ==null || mValues.isEmpty()) {
            fetchSchoolList();
        }else{
            if(adapter != null)
                adapter.notifyDataSetChanged();
        }
    }

    public void fetchSchoolList() {
        NYCServiceProvider apiServiceProvider = NYCServiceProvider.getInstance(this);
        apiServiceProvider.getSchoolList(this);
    }


    private void setupRecyclerView(@NonNull RecyclerView recyclerView,ArrayList<NYCSchoolListInfo> list) {
        adapter = new SimpleItemRecyclerViewAdapter(this, list, mTwoPane);
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, list, mTwoPane));
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), VERTICAL));
    }

    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.schoolinfo_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), VERTICAL));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final SchoolInfoListActivity mParentActivity;
       // private final List<DummyContent.DummyItem> mValues;

        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NYCSchoolListInfo item = (NYCSchoolListInfo) view.getTag();
                if (mTwoPane) {
                    SchoolInfoDetailFragment schoolDetailFragment = new SchoolInfoDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(SchoolInfoDetailFragment.ARG_ITEM_ID,item);
                    schoolDetailFragment.setArguments(bundle);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.schoolinfo_detail_container, schoolDetailFragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, SchoolInfoDetailActivity.class);
                    intent.putExtra(Constants.SELECTED_SCHOOL_INFO, item);
                    context.startActivity(intent);
                }
            }
        };

        SimpleItemRecyclerViewAdapter(SchoolInfoListActivity parent,
                                      ArrayList<NYCSchoolListInfo> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.schoolinfo_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            holder.schoolName.setTag(mValues.get(position));
            holder.schoolName.setText(mValues.get(position).getSchoolName());
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView schoolName;

            ViewHolder(View view) {
                super(view);
                schoolName = view.findViewById(R.id.school_name);

            }
        }
    }

    @Override
    public void onResponseSuccess(Response response, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.NYC_SCHOOL_LIST:
                ArrayList<NYCSchoolListInfo> listLiveData = (ArrayList<NYCSchoolListInfo>)response.body();
                setupRecyclerView(recyclerView,listLiveData);
                break;
            case Constants.ApiFlags.NYC_SCHOOL_DETAILS:
                List<NYCSchoolDetailInfo> detail = (List<NYCSchoolDetailInfo>)response.body();

                break;
        }
    }

    @Override
    public void onResponseError(ErrorObject errorObject, Throwable throwable, int apiFlag) {
        switch (apiFlag) {
            case Constants.ApiFlags.NYC_SCHOOL_LIST:
                //Toast.makeText(this,"SchoolList: error response: " + new Gson().toJson(errorObject),Toast.LENGTH_SHORT).show();
                break;
            case Constants.ApiFlags.NYC_SCHOOL_DETAILS:
                //Toast.makeText(this,"SchoolDetail: error response: " + new Gson().toJson(errorObject),Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
